const path = require('path');
// const favicon = require('serve-favicon');
const compress = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const logger = require('./logger');
// const logger = require('winston');
const morgan = require('morgan');

const bodyParser = require('body-parser');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');


// const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');

const mongodb = require('./mongodb');

const app = module.exports = express(feathers());

// Load app configuration
app.configure(configuration());

(async function () {
  app.configure(mongodb);
  app.locals.db = await app.get('mongoClient');

  app.use(morgan(':date :remote-addr :method :url :status :response-time'));

  // app.use(cookieParser());
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  app.use(bodyParser.json());
  // Parse URL-encoded params
  app.use(bodyParser.urlencoded({ extended: true }));

  // Enable security, CORS, compression, favicon and body parsing
  app.use(helmet());
  app.use(cors());
  app.use(compress());
  // Host the public folder
  app.use('/', express.static(app.get('public')));

  // Set up Plugins and providers
  app.configure(express.rest());
  app.configure(socketio());
  // app.configure(
  //   socketio(19046, { path: '/myapp2/socket.io' }, require('./scripts/socket/index.js')({
  //     app, pug
  //   })
  // ));

  // // Configure other middleware (see `middleware/index.js`)
  // app.configure(middleware);
  // Set up our services (see `services/index.js`)
  app.configure(services);
  // Set up event channels (see channels.js)
  app.configure(channels);

  app.hooks(appHooks);

  // app.locals.isDev = process.env.NODE_ENV != 'production';
  // logger.info('Development mode:', app.locals.isDev);

  // Configure a middleware for 404s and the error handler
  app.use(express.notFound());
  app.use(express.errorHandler({ logger }));

  const port = app.get('port');
  const server = app.listen(port);

  process.on('unhandledRejection', (reason, p) =>
    logger.error('Unhandled Rejection at: Promise ', p, reason)
  );

  server.on('listening', () =>
    logger.info('Feathers application started on http://%s:%d', app.get('host'), port)
  );
})();
