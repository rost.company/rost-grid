const assert = require('assert');
const app = require('../../core/app');

describe('\'orders\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/orders');

    assert.ok(service, 'Registered the service');
  });
});
