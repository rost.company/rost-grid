const MongoClient = require('mongodb').MongoClient;

const config = 'mongodb://localhost:27017/rost_grid';
const promise = MongoClient.connect(config, { useNewUrlParser: true });

const moment = require('moment');

const data = require('./data'); // мои данные

promise.then(async client => {
  const db = client.db('rost_grid');
  const orders = await db.collection('orders').find().toArray();

  const count = 1000; // кол-во заказов

  for (let i = 0; i < count; i++) {
    const obj = {}; // объект с данными о заказе
    // Math.floor(Math.random()*(max-min+1)+min)
    const firstName = data.firstNames[Math.floor(Math.random() * data.firstNames.length)];
    const lastName = data.lastNames[Math.floor(Math.random() * data.lastNames.length)];
    obj.createdDate = new Date(moment().subtract(count - Math.floor(i / 10), 'days')); // дата создания заказа, начиная от кол-ва заказов деленное на 10 дней назад по 10 заказов в день
    obj.name = firstName + ' ' + lastName; // полное имя

    const user = {}; // объект с данными о пользователе
    user.firstName = firstName;
    user.lastName = lastName;
    user.phone = '';
    for (let j = 0; j < 10; j++) {
      user.phone += Math.floor(Math.random() * 10); // 10 рандомных цифр
    }
    user.email = '';
    const possible = 'abcdefghijklmnopqrstuvwxyz0123456789'; // символы для почты
    for (let j = 0; j < 10; j++)
      user.email += possible.charAt(Math.floor(Math.random() * possible.length)); // рандомный логин почты
    user.email += '@';
    for (let j = 0; j < 5; j++)
      user.email += possible.slice(0, -10).charAt(Math.floor(Math.random() * possible.slice(0, -10).length)); // рандомный домен, состоящий только из букв
    if (i % 3 === 0) {
      user.email += '.com';
    } else if (i % 3 === 1) {
      user.email += '.info';
    } else if (i % 3 === 2) {
      user.email += '.net';
    }
    obj.user = user;

    const products = []; // массив товаров в заказе
    const countOfProducts = Math.floor(Math.random() * 5 + 1); // рандомное кол-во заказанных разных товаров
    for (let j = 0; j < countOfProducts; j++) {
      const product = data.products[Math.floor(Math.random() * data.products.length)]; // рандомный товар
      product.count = Math.floor(Math.random() * 10 + 1); // рандомное кол-во единиц товара от 1 до 10
      let founded = false;
      for (let k = 0; k < products.length; k++) {
        if (product.sku === products[k].sku) {
          founded = true;
        }
      }
      if (!founded) {
        products.push(product);
      }
    }
    obj.products = products;

    let productsCount = 0;
    let productsSumm = 0;
    for (let j = 0; j < products.length; j++) {
      productsCount += products[j].count;
      productsSumm += products[j].price * products[j].count;
    }
    obj.count = productsCount; // кол-во всех заказанных товаров
    obj.summ = productsSumm; // сумма (денег) всех заказанных товаров

    obj.status = data.statuses[Math.floor(Math.random() * data.statuses.length)]; // статус заказа
    obj.orderID = 1001 + i; // уникальный номер заказа

    await db.collection('orders').insertOne(obj);
  }
  
  console.log('OK')
  process.exit();
});
