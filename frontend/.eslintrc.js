// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
    ecmaVersion: 7,
    ecmaFeatures: {
      modules: true,
      jsx: true
    }
  },
  plugins: [
    'react'
  ],
  env: {
    browser: true,
    node: true,
    es6: true
  },
  extends: 'eslint:recommended',
  // add your custom rules here
  rules: {
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'indent': ['error', 2],
    'semi': ['error', 'always'],
    'no-multiple-empty-lines': 'error',
    'jsx-quotes': ['error', 'prefer-single'],
    'quotes': ['error', 'single']
  }
}
