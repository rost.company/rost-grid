import { createStore, applyMiddleware, combineReducers } from 'redux';
import { apiMiddleware } from 'redux-api-middleware';
import { rootReducer, initialState } from '../reducers'
// import reducers from './reducers';

// const reducer = combineReducers(rootReducer);
const createStoreWithMiddleware = applyMiddleware(apiMiddleware)(createStore);
const store = createStoreWithMiddleware(rootReducer, initialState)
// store.subscribe(function(){
// 	console.log(store.getState())
// })

if (module.hot) {
    module.hot.accept('../reducers', function () {
        const nextRootReducer = require('../reducers').rootReducer
        store.replaceReducer(nextRootReducer)
    })
}

export default store
