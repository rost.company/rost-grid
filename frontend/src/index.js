import React from 'react'
import ReactDOM from 'react-dom'
import App from './containers/App'
import { Provider } from 'react-redux'
import store from './store'

/* enable react hotreload + webpack                */
/* see https://github.com/gaearon/react-hot-loader */
if (module.hot) {
    module.hot.accept()
}

// mount app container
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
)