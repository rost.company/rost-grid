## Development

``` bash
# install dependencies
npm i

# dev server with hot reload at localhost:3000
npm start

# dev server with hot reload at localhost:3000 without info
npm run dev

# build for production with minification
npm run build

# build for with minification and progress
npm run build:progress

# build for production with minification and progress
npm run build:production

# run tests
npm run test

# run tests and watch progress all test
npm run test:watch

# build tests coverage report
npm run test:coverage
```